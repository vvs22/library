package ua.hillel.implemetation;

import org.h2.jdbc.JdbcSQLInvalidAuthorizationSpecException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Auth {
    private String userName;
    private String userPassword;
    private Scanner myScanner = new Scanner(System.in);


    public String authInLibrary(String url) {
        boolean isAuth = false;
        while (!isAuth) {
            System.out.println("В системе зарегистрировано три пользователя: reader1 - пароль 1; reader2 - пароль 2; librarian - пароль 123");
            System.out.println("Представтесь пожалуйста: ");
            try {
                userName = myScanner.next();
            } catch (InputMismatchException e) {
                userName = null;
            }
            System.out.println("Введите пароль: ");
            try {
                userPassword = myScanner.next();
            } catch (InputMismatchException e) {
                userPassword = null;
            }
//            System.out.println("Попытаемся подключиться к базе: " + userName + ":" + userPassword);
            try {
                Class.forName("org.h2.Driver");
                Connection conn = DriverManager.getConnection(url, userName, userPassword);
            } catch (JdbcSQLInvalidAuthorizationSpecException e) {
                System.out.println("Ошибка покдлючения к Библиотеке!!! Попробуйте ещё раз");
                userName = null;
                userPassword = null;
            } catch (SQLException e) {
                userName = null;
                userPassword = null;
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                userName = null;
                userPassword = null;
                e.printStackTrace();
            }
            if (userName != null && userPassword != null) {
                isAuth = true;
                this.userName = userName;
                this.userPassword = userPassword;
            }
        }
        return userName;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserPassword() {
        return userPassword;
    }
}
