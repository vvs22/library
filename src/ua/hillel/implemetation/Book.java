package ua.hillel.implemetation;

import org.h2.jdbc.JdbcSQLInvalidAuthorizationSpecException;

import java.sql.*;

public class Book {
    private int idBook;
    private String nameBook;
    private String indexBook;
    private Boolean inLibrary;
    private Boolean onReading;
    private String readerBook;
    private String maintainerBook;
    private java.sql.Date data;

    public void getBookFromLibrary(String url, String userName, String userPassword, int idBook) {
        if (idBook <= 0) {
            idBook = 1;
        }
        try {
            Class.forName("org.h2.Driver");
            Connection conn = DriverManager.getConnection(url, userName, userPassword);
            Statement st = null;
            st = conn.createStatement();
            ResultSet resultSet;
            resultSet = st.executeQuery("SELECT * FROM LIBRARY WHERE ID=" + idBook);
            while (resultSet.next()) {
                idBook = resultSet.getInt(1);
                nameBook = resultSet.getString(2);
                indexBook = resultSet.getString(3);
                inLibrary = resultSet.getBoolean(4);
                onReading = resultSet.getBoolean(5);
                readerBook = resultSet.getString(6);
                maintainerBook = resultSet.getString(7);
                data = resultSet.getDate(8);
                System.out.println(idBook + " " + nameBook + " " + indexBook + " " + inLibrary + " " + onReading + " " + readerBook + " " + maintainerBook + " " + data);
                extractBook(url, userName, userPassword, idBook);
            }
        } catch (JdbcSQLInvalidAuthorizationSpecException e) {
            System.out.println("Ошибка покдлючения к Библиотеке!!!");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void extractBook(String url, String userName, String userPassword, int idBook) {
        try {
            long millis = System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(millis);
            Class.forName("org.h2.Driver");
            Connection conn = DriverManager.getConnection(url, userName, userPassword);
            Statement st = null;
            st = conn.createStatement();
            String query = "UPDATE library SET INLIBRARY = false, ONREADING = true, READER = ?, DATA_RECORD=? WHERE ID = ?";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, userName);
            pstmt.setString(2, String.valueOf(date));
            pstmt.setString(3, String.valueOf(idBook));
            pstmt.execute();
        } catch (JdbcSQLInvalidAuthorizationSpecException e) {
            System.out.println("Ошибка покдлючения к Библиотеке!!!");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void addBookToLibrary(String url, String userName, String userPassword, String nameBook, String indexBook, boolean inLibrary, boolean onReading, String readerBook, String maintainerBook, Date data) {
        try {
            Class.forName("org.h2.Driver");
            Connection conn = DriverManager.getConnection(url, userName, userPassword);
            Statement st = null;
            st = conn.createStatement();
            ResultSet resultSet;
            int idNextBook;
            resultSet = st.executeQuery("SELECT max(ID) FROM LIBRARY");
            while (resultSet.next()) {
                idNextBook = resultSet.getInt(1);
                idBook = idNextBook + 1;
            }
            long millis = System.currentTimeMillis();
            java.sql.Date date = new java.sql.Date(millis);
            String query = "INSERT INTO library VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setString(1, String.valueOf(idBook));
            pstmt.setString(2, nameBook);
            pstmt.setString(3, indexBook);
            pstmt.setString(4, String.valueOf(inLibrary));
            pstmt.setString(5, String.valueOf(onReading));
            pstmt.setString(6, readerBook);
            pstmt.setString(7, maintainerBook);
            pstmt.setString(8, String.valueOf(date));
            pstmt.execute();
        } catch (JdbcSQLInvalidAuthorizationSpecException e) {
            System.out.println("Ошибка покдлючения к Библиотеке!!!");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void printAllBooksInLibrary(String url, String userName, String userPassword) {
        try {
            Class.forName("org.h2.Driver");
            Connection conn = DriverManager.getConnection(url, userName, userPassword);
            Statement st = null;
            st = conn.createStatement();
            ResultSet resultSet = st.executeQuery("select * from library");
            while (resultSet.next()) {
                idBook = resultSet.getInt(1);
                nameBook = resultSet.getString(2);
                indexBook = resultSet.getString(3);
                inLibrary = resultSet.getBoolean(4);
                onReading = resultSet.getBoolean(5);
                readerBook = resultSet.getString(6);
                maintainerBook = resultSet.getString(7);
                data = resultSet.getDate(8);
//                System.out.println(idBook + " " + nameBook + " " + indexBook + " " + data);
                System.out.println(idBook + " " + nameBook + " " + indexBook + " " + inLibrary + " " + onReading + " " + readerBook + " " + maintainerBook + " " + data);
            }
        } catch (JdbcSQLInvalidAuthorizationSpecException e) {
            System.out.println("Ошибка покдлючения к Библиотеке!!!");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public void printCurrentBooksInLibrary(String url, String userName, String userPassword) {
        try {
            Class.forName("org.h2.Driver");
            Connection conn = DriverManager.getConnection(url, userName, userPassword);
            Statement st = null;
            st = conn.createStatement();
            ResultSet resultSet = st.executeQuery("select * from library where inLibrary=true");
            while (resultSet.next()) {
                idBook = resultSet.getInt(1);
                nameBook = resultSet.getString(2);
                indexBook = resultSet.getString(3);
                inLibrary = resultSet.getBoolean(4);
                onReading = resultSet.getBoolean(5);
                readerBook = resultSet.getString(6);
                maintainerBook = resultSet.getString(7);
                data = resultSet.getDate(8);
//                System.out.println(idBook + " " + nameBook + " " + indexBook + " " + data);
                System.out.println(idBook + " " + nameBook + " " + indexBook + " " + inLibrary + " " + onReading + " " + readerBook + " " + maintainerBook + " " + data);
            }
        } catch (JdbcSQLInvalidAuthorizationSpecException e) {
            System.out.println("Ошибка покдлючения к Библиотеке!!!");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
