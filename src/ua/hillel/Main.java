package ua.hillel;

import ua.hillel.initlibrary.InitLibrary;
import ua.hillel.implemetation.Auth;
import ua.hillel.implemetation.Book;

public class Main {
    public static void main(String[] args) {
        System.out.println("Добро пожаловать в Библиотеку!");
        String url = "jdbc:h2:mem:~/library";
        InitLibrary initLibrary = new InitLibrary();
        try {
            initLibrary.initLibrary(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Auth authUser = new Auth();
        String userName = authUser.authInLibrary(url);
        System.out.println("Вы зашли в библиотеку под именем: " + userName);
        Book book = new Book();
        book.printAllBooksInLibrary(url, authUser.getUserName(), authUser.getUserPassword());
        System.out.println("Попробуем взять 7-мую книгу ...");
        book.getBookFromLibrary(url, authUser.getUserName(), authUser.getUserPassword(), 7);
        System.out.println("Покажем доступные книги в библиотеке: ");
        book.printCurrentBooksInLibrary(url,authUser.getUserName(),authUser.getUserPassword());
        System.out.println("Покажем все книги в библиотеке: ");
        book.printAllBooksInLibrary(url, authUser.getUserName(), authUser.getUserPassword());

    }
}
