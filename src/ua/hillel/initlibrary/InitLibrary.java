package ua.hillel.initlibrary;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;

import ua.hillel.implemetation.Book;

public class InitLibrary {

    public void initLibrary(String url) {
        try {
            Class.forName("org.h2.Driver");
            Connection conn = DriverManager.getConnection(url, "sa", "sa");
            Statement st = null;
            st = conn.createStatement();
            st.execute("CREATE USER IF NOT EXISTS reader1 PASSWORD '1'");
            st.execute("CREATE USER IF NOT EXISTS reader2 PASSWORD '2'");
            st.execute("CREATE USER IF NOT EXISTS librarian PASSWORD '123'");
            st.execute("CREATE USER IF NOT EXISTS Admin PASSWORD 'Admin' ADMIN");
            st.execute("DROP TABLE IF EXISTS LIBRARY;");
            st.execute("CREATE TABLE LIBRARY(ID INT PRIMARY KEY, NAME VARCHAR(255), INDEX VARCHAR(255), INLIBRARY BOOLEAN, ONREADING BOOLEAN, READER VARCHAR(255), MAINTAINER VARCHAR(255), DATA_RECORD DATE)");
            st.execute("GRANT ALL ON LIBRARY TO PUBLIC");
        } catch (
                Exception e) {
            e.printStackTrace();
        }
        long millis = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);
        Book bookToAdd = new Book();
        bookToAdd.addBookToLibrary(url, "reader1", "1", "JAVA DOCS 1", "ISBN ......", true, false, null, "Admin", date);
        bookToAdd.addBookToLibrary(url, "reader1", "1", "JAVA DOCS 2", "ISBN ......", true, false, null, "Admin", date);
        bookToAdd.addBookToLibrary(url, "reader1", "1", "JAVA DOCS 3", "ISBN ......", true, false, null, "Admin", date);
        bookToAdd.addBookToLibrary(url, "reader1", "1", "JAVA DOCS 4", "ISBN ......", true, false, null, "Admin", date);
        bookToAdd.addBookToLibrary(url, "reader1", "1", "JAVA DOCS 5", "ISBN ......", true, false, null, "Admin", date);
        bookToAdd.addBookToLibrary(url, "reader1", "1", "JAVA DOCS MANUAL 1", "ISBN ......", true, false, null, "Admin", date);
        bookToAdd.addBookToLibrary(url, "reader1", "1", "JAVA DOCS MANUAL 2", "ISBN ......", true, false, null, "Admin", date);
        bookToAdd.addBookToLibrary(url, "reader1", "1", "JAVA DOCS MANUAL 3", "ISBN ......", true, false, null, "Admin", date);
        bookToAdd.addBookToLibrary(url, "reader1", "1", "JAVA DOCS MANUAL 4", "ISBN ......", true, false, null, "Admin", date);
        bookToAdd.addBookToLibrary(url, "reader1", "1", "JAVA DOCS MANUAL 5", "ISBN ......", true, false, null, "Admin", date);
    }
}
